#!/usr/bin/env python3
# coding=utf-8

"""
    tests.py
    ~~~~~~~~

    Executes test cases for the workflow.

    :copyright: (c) 2017 by Ali Ghaffaari.
    :license: MIT, see LICENSE for more details.
"""

__author__ = "Ali Ghaffaari"
__license__ = "MIT"


import shutil
from os.path import exists

from snakemake import snakemake


if __name__ == "__main__":
    exp_dir = "test/experiments"
    if exists(exp_dir):
        print("Cleaning up...")
        shutil.rmtree(exp_dir)

    print("Running workflow...")
    if snakemake("Snakefile", workdir="."):
        print("PASSED")
    else:
        print("FAILED")
