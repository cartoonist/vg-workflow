# coding=utf-8

"""Variation graph construction workflow.

This Snakefile requires a configuration file having at least these entries:

    ---
    datasets:
      - "dataset1"
      - "dataset2"
      - ...

    # Either specify input genome sequence for each dataset:
    genome:
      dataset1: "/path/to/its_genome.fa"
      dataset2: "/path/to/another_genome.fasta"
      ...
    # Or use dataset wildcard (pay attention to trailing 's' in genome*s*).
    dataset_genomes: "/path/to/{dataset}/genome.fa[sta]"

    # Either specify input variants for each dataset:
    variants:
      dataset1: "/path/to/variants.vcf.gz"
      dataset2: "/path/to/variants2.vcf.gz"
      ...
    # Or use wildcard (it can also be optionally parameterized by {region}).
    dataset_variants: "/path/to/{dataset}/{region}.vcf.gz"

    targets:
      - "/path/to/TARGET_A"
      - "/another/path/to/TARGET_B"
      - ...
    ...

where `TARGET_A` or `TARGET_B` can be EXACTLY one of the final targets:

- "report.html"             for generating the experiment report.
- "{dataset}/wg.gcsa"       for constructing GCSA index for whole genome graph.
- "{dataset}/wg.xg"         for constructing xg index for whole genome graph.
- "{dataset}/rg.xg.done"    for constructing per-region xg index.

or intermediate targets which skip indexing:

- "{dataset}/wg.vg"         for constructing whole genome graph;
                            also implied by 'wg.gcsa' or 'wg.xg' targets.

Also, whole genome graph or region graphs of a dataset can be converted to
handle-graph complaint formats `vg`, `pg`, and `hg`. To do so, add
corresponding targets to the target list:

- "{dataset}/wg.vg.vg"         convert whole genome graph to Protobuf vg,
- "{dataset}/wg.pg.vg"         convert whole genome graph to PackedGraph,
- "{dataset}/wg.hg.vg"         convert whole genome graph to HashGraph,
- "{dataset}/rg.vg.vg"         convert all region graphs to Protobuf vg,
- "{dataset}/rg.pg.vg"         convert all region graphs to PackedGraph,
- "{dataset}/rg.hg.vg"         convert all region graphs to HashGraph.

The `{dataset}` pattern will be expanded for all datasets specified in
configuration file or can be manually stated; e.g "/path/to/ecoli/wg.xg".

There is also a sample configuration file in the root directory.

For more information about the workflow see README file.
"""

__author__ = "Ali Ghaffaari"
__email__ = "ali.ghaffaari@mpi-inf.mpg.de"
__organization__ = "Max-Planck Institut fuer Informatik"
__license__ = "MIT"
__version__ = "v2.3.4"


# Using `bash` for shell commands.
shell.executable("/bin/bash")
# Check for Snakemake minimum version.
snakemake.utils.min_version("3.9.0")

configfile: "config.yml"

WORKFLOW_PREFIX = "https://bitbucket.org/cartoonist/vg-workflow/raw/" + __version__
WRAPPERS_PREFIX = "https://github.com/cartoonist/snakemake-wrappers/raw/develop"
BLOB_TOKEN = 'src'  # GitHub: 'blob', BitBucket: 'src', Local: ''

# Fetch targets
TARGETS = expand(config["targets"], dataset=config["datasets"])

##  Rules
rule all:
    input:
        TARGETS

include:
    WORKFLOW_PREFIX + "/rules/vg.rules"

include:
    WORKFLOW_PREFIX + "/rules/report.rules"
