Variation Graph Workflow
========================

This workflow uses Snakemake to automate the process of variation graph
construction, and indexing the constructed graph.

The snakefile creates and index the variation graph for the given datasets using
variants observed in the given samples in each dataset with respect to the
reference genome. Figure [dependency DAG](figures/snakefile_dag.svg) illustrates
the rule dependency DAG representing different stages of the workflow:

![dependency DAG](figures/snakefile_dag.png)

---

**NOTE**

All parameters for each experiment should be provided by a config file. Don't
change the Snakefile for this purpose. There is an example config file in the
root directory.

---

To get more information about an experiment, and detailed explanation of the
workflow stages see the generated report file in HTML located in experiment
directory. For more information about the parameters, consult with comments in
sample config file for each parameter.

Preparing
---------
It would be more convenient if the snakefile is executed in an isolated Python
environment. In order to create and activate a python virtual environment using
`virtualenv` run:

```bash
virtualenv -p python3 venv
source ./venv/bin/activate
```

In order to execute the workflow, some python packages are required to be
installed. These packages are listed in the [requirements.txt](requirements.txt)
file and could be installed by running:

```bash
pip install -r requirements.txt
```

Usage
-----
Each experiment has its own config file describing that experiment. In order to
keep the experiments fully reproducible, it might be a good idea to keep the
Snakefile in the experiment directory too. After writing the config file and
putting Snakefile in the same directory, execute it by running:

```bash
snakemake [--jobs/-j <N>]
```
